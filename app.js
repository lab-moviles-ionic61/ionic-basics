const tipoInput = document.querySelector('#tipo');
const montoInput = document.querySelector('#monto');
const btnInput = document.querySelector('#boton');
const gastosList = document.querySelector('#list-gastos');
const totalOutput = document.querySelector('#output');

let total = 0;

function clear(){
    tipoInput.value = '';
    montoInput.value = '';
}

btnInput.addEventListener('click',() => {
    console.log(tipoInput.value);
    const tipo = tipoInput.value;
    const monto = montoInput.value;

    if(tipo.trim().length > 0 && monto.trim().length > 0 && monto > 0) {
        console.log(tipo, monto);
        const newItem = document.createElement('ion-item');
        newItem.textContent = tipo + ': $' + monto;
        gastosList.appendChild(newItem);

        total += +monto;
        totalOutput.textContent = total;

        clear();
    }
    else {
        console.error('Valores invalidos !');
        alertController.create({
            message: 'Llene los campos correctamente',
            header: 'Valores invalidos',
            buttons: ['Ok']
        }).then(alertElement => {
            alertElement.present();
        });
    }
});

